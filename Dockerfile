FROM python:3.10

# Setup workdir.
WORKDIR /code/

# Install dependancies.
RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | POETRY_HOME=/opt/poetry python && \
    cd /usr/local/bin && \
    ln -s /opt/poetry/bin/poetry && \
    poetry config virtualenvs.create false

COPY ./pyproject.toml ./poetry.lock* /code/

ARG INSTALL_DEV=false
RUN bash -c "if [ $INSTALL_DEV == 'true' ] ; then poetry install --no-root ; else poetry install --no-root --no-dev ; fi"

# Copy code.
COPY . /code
ENV PYTHONPATH=/code

EXPOSE 8000

CMD uvicorn api.main:app --host=0.0.0.0
