# Tasks

Before running make sure you have docker and docker-compose installed.

## Running

```bash
docker-compose build
docker-compose up
```

Visit http://localhost:8000/docs to interact with the api.

## Run tests

```bash
docker-compose run api pytest
```
