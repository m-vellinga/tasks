from http import HTTPStatus
from uuid import UUID

from fastapi import APIRouter, Depends, HTTPException, Path

from api.core.abstracts import ABCTaskRepository
from api.core.entities import TaskEntity
from api.data.exceptions import NotFoundError
from api.injector import task_repository

from .schemas import TaskCreateSchema, TaskListSchema, TaskPatchSchema, TaskSchema


router = APIRouter()


@router.post("", status_code=HTTPStatus.CREATED.value, response_model=TaskSchema)
async def create_task(create_data: TaskCreateSchema, task_repository: ABCTaskRepository = Depends(task_repository)):
    task = TaskEntity.from_schema(create_data)

    await task_repository.create(task)

    return TaskSchema.from_orm(task)


@router.get("", response_model=TaskListSchema)
async def list_tasks(task_repository: ABCTaskRepository = Depends(task_repository)):
    task_list = await task_repository.list()

    return TaskListSchema(items=list(map(TaskSchema.from_orm, task_list)))


@router.get("/{task_id}", response_model=TaskSchema)
async def get_task(task_id: UUID = Path(...), task_repository: ABCTaskRepository = Depends(task_repository)):
    try:
        task = await task_repository.get(id=task_id)
    except NotFoundError:
        raise HTTPException(status_code=HTTPStatus.NOT_FOUND.value)
    else:
        return TaskSchema.from_orm(task)


@router.patch("/{task_id}", response_model=TaskSchema)
async def patch_task(
    patch_data: TaskPatchSchema,
    task_id: UUID = Path(...),
    task_repository: ABCTaskRepository = Depends(task_repository),
):
    """
    View for patching an existing task. Only allowed to patch `title` and `is_done`.
    """
    try:
        task = await task_repository.get(task_id)
    except NotFoundError:
        raise HTTPException(status_code=HTTPStatus.NOT_FOUND.value)

    task.update(**patch_data.dict(exclude_unset=True))

    await task_repository.update(task)

    return TaskSchema.from_orm(task)
