from typing import List, Optional
from uuid import UUID

from pydantic import BaseModel, root_validator


class TaskSchema(BaseModel):
    id: UUID
    title: str
    is_done: bool

    class Config:
        """
        Enable the .from_orm method for easy mapping from
        database model to schema.
        """

        orm_mode = True


class TaskCreateSchema(BaseModel):
    title: str


class TaskListSchema(BaseModel):
    """
    Seperate schema to have a root element called items. Makes it
    more extensible in the future.
    """

    items: List[TaskSchema]


class TaskPatchSchema(BaseModel):
    title: Optional[str]
    is_done: Optional[bool]

    @root_validator
    def atleast_one_required(cls, data):
        if not any(data.values()):
            raise ValueError("Provide atleast one field to patch")
        return data
