import uuid
from http import HTTPStatus
from unittest.mock import Mock

from api.core.abstracts import ABCTaskRepository
from api.data.exceptions import NotFoundError
from api.injector import task_repository


async def test_task_get_not_found(test_client):
    """
    Test getting a non existing task returns a HTTP404.

    We are not interested in the database logic just that a 404 is returned on a NotFoundError
    so we can safely mock the data layer.
    """
    mock = Mock(spec=ABCTaskRepository)
    mock.get.side_effect = NotFoundError()

    test_client.app.dependency_overrides[task_repository] = lambda: mock

    non_existing_task_id = uuid.uuid4()
    response = await test_client.get(f"/v1/task/{non_existing_task_id}")

    mock.get.assert_called_once()
    assert response.status_code == HTTPStatus.NOT_FOUND.value
