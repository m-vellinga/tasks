import httpx
import pytest

from .main import app


@pytest.fixture
async def test_client():
    client = httpx.AsyncClient(app=app, base_url="http://testserver")

    setattr(client, "app", app)

    yield client

    app.dependency_overrides = {}

    await client.aclose()
