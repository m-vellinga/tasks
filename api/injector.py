"""
Use for FastAPI's buildin dependency injection system.
"""

from fastapi import Depends
from sqlalchemy.ext.asyncio import AsyncSession

from .config import main_config
from .data.db import DbSessionFactory
from .data.repositories import TaskRepository


db_session_factory = DbSessionFactory(postgres_dsn=main_config.POSTGRES_DSN)


async def db_session():
    """
    Create a database session that is tied to a single request/response cycle,
    cleans up and commits or rolls back the database transaction.
    """
    session = db_session_factory.new_session()
    try:
        yield session
    except Exception:
        await session.rollback()
    else:
        await session.commit()
    finally:
        await session.close()


def task_repository(db_session_obj: AsyncSession = Depends(db_session)):
    return TaskRepository(db_session=db_session_obj)
