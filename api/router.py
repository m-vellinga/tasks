from fastapi import APIRouter

from .endpoints.views import router as task_router


v1_router = APIRouter()

v1_router.include_router(task_router, prefix="/task", tags=["task"])
