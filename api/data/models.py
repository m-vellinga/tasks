import uuid

from sqlalchemy import Boolean, Column, Integer, String
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.declarative import as_declarative


@as_declarative()
class BaseDbModel:
    """
    To ensure automatic generated migrations work with alembic.
    """

    pass


class TaskDbModel(BaseDbModel):
    __tablename__ = "task"

    pk = Column(Integer, primary_key=True, index=True)
    id = Column(
        UUID(as_uuid=True),
        unique=True,
        nullable=False,
        index=True,
        default=uuid.uuid4,
    )

    title = Column(String, nullable=False)
    is_done = Column(Boolean, default=False, nullable=False)

    @classmethod
    def from_entity(cls, entity_object):
        """
        Helper for easy mapping from entity to db model.
        """
        return cls(**entity_object.dict())
