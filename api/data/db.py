from asyncio import current_task

from pydantic.networks import PostgresDsn
from sqlalchemy.ext.asyncio import AsyncSession, async_scoped_session, create_async_engine
from sqlalchemy.orm import sessionmaker


class DbSessionFactory:
    def __init__(self, postgres_dsn: PostgresDsn):
        engine = create_async_engine(
            postgres_dsn,
            pool_size=20,
            max_overflow=0,
            pool_pre_ping=True,
        )
        async_session_maker = sessionmaker(engine, autoflush=True, class_=AsyncSession)
        self._factory = async_scoped_session(async_session_maker, scopefunc=current_task)

    def new_session(self) -> AsyncSession:
        return self._factory()
