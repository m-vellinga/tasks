from typing import List
from uuid import UUID

from sqlalchemy import update
from sqlalchemy.exc import NoResultFound
from sqlalchemy.ext.asyncio.session import AsyncSession
from sqlalchemy.future import select

from api.core.abstracts import ABCTaskRepository
from api.core.entities import TaskEntity

from .exceptions import NotFoundError
from .models import TaskDbModel


class TaskRepository(ABCTaskRepository):
    def __init__(self, db_session: AsyncSession):
        self.db_session = db_session

    async def create(self, task: TaskEntity):
        task = TaskDbModel.from_entity(task)

        self.db_session.add(task)

        await self.db_session.flush()
        await self.db_session.refresh(task)

    async def get(self, id: UUID) -> TaskEntity:
        try:
            task = (await self.db_session.execute(select(TaskDbModel).filter_by(id=id))).scalar_one()
        except NoResultFound:
            raise NotFoundError

        return TaskEntity.from_model(task)

    async def list(self) -> List[TaskEntity]:
        tasks = (await self.db_session.execute(select(TaskDbModel))).scalars().all()
        return list(map(TaskEntity.from_model, tasks))

    async def update(self, task: TaskEntity):
        result = await self.db_session.execute(
            update(
                TaskDbModel,
            )
            .where(TaskDbModel.id == task.id)
            .values(
                title=task.title,
                is_done=task.is_done,
            ),
        )
        if result.rowcount == 0:
            raise NotFoundError
