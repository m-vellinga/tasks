from pydantic import BaseSettings
from pydantic.networks import PostgresDsn


class MainConfig(BaseSettings):
    POSTGRES_DSN: PostgresDsn


main_config = MainConfig()
