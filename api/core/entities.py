from uuid import UUID, uuid4

from pydantic import BaseModel, Field


class TaskEntity(BaseModel):
    id: UUID = Field(default_factory=uuid4)
    title: str
    is_done: bool = False

    @classmethod
    def from_model(cls, model_object):
        """
        Helper function to map a model to an entity.
        """
        return cls.from_orm(model_object)

    @classmethod
    def from_schema(cls, schema_object):
        """
        Helper function to map a schema to an entity.
        """
        return cls.from_orm(schema_object)

    def update(self, **kwargs):
        """
        Helper function for easy updating.
        """
        for field_name, value in kwargs.items():
            setattr(self, field_name, value)

    class Config:
        orm_mode = True
        validate_assignment = True
