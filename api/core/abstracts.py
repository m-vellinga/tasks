from abc import ABC, abstractmethod
from typing import List
from uuid import UUID

from .entities import TaskEntity


class ABCTaskRepository(ABC):
    @abstractmethod
    async def get(self, id: UUID) -> TaskEntity:
        pass

    @abstractmethod
    async def list(self) -> List[TaskEntity]:
        pass

    @abstractmethod
    async def create(self, task: TaskEntity):
        pass

    @abstractmethod
    async def update(self, task: TaskEntity):
        pass
