from fastapi import FastAPI

from .router import v1_router


app = FastAPI(
    title="Tasks API",
    description="Simple tasks list so that radiologists don't forget them.",
    version="0.1.0",
)

app.include_router(v1_router, prefix="/v1")
